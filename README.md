# MLLP ASR test application

## MLLPStreamingClient_mllp-1.0.0-py3-none-any.whl

Put the library on the vendor folder

## Settings

Populate the config.py file with the API values

## Install

```bash
python -m venv .venv
source .venv/bin/activate
pip install -U pip
pip install -r requirements.txt
```

## Preparing an audio stream

> ffmpeg required

### Get a mp4 from HLS

```bash
ffmpeg -i https://wowza.cern.ch/vod/_definist_/smil:Video/Public/WebLectures/2017/635414c2/635414c2_desktop_slides.smil/playlist.m3u8 slides.mp4
```

### Get the wav audio in the correct format

```bash
ffmpeg -i slides.mp4 -ac 1 -acodec pcm_s16le -ar 16000 slides-audio.wav
```

## Run

```bash
python -m app.py
```

