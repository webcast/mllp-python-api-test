import json
import os
import sys

from MLLPStreamingClient import MLLPStreamingClient

import config

print("Starting MLLP Streaming API: https://ttp.mllp.upv.es/index.php?page=api")

server_hostname = "ttp.mllp.upv.es"
server_port = "47110"

current_path = os.path.dirname(os.path.realpath(__file__))
certificate_path = os.path.join(current_path, "ttp.mllp.upv.es2.crt")
wav_file = os.path.join(current_path, "./test2.wav")
output_transcription = os.path.join(current_path, "./transcript.txt")

print("Certificate path is: {}".format(certificate_path))

cli = MLLPStreamingClient(server_hostname, server_port, config.API_USER,
                          config.API_SECRET, certificate_path)

print("Obtaining token from the MLLP Client...")
cli.getAuthToken()

systems = cli.getTranscribeSystemsInfo()
print(json.dumps(systems, indent=4))


def myStreamIterator():
    with open(wav_file, "rb") as fd:
        data = fd.read(250)
        while data != b"":
            yield data
            data = fd.read(250)

for resp in cli.transcribe(systems[0]["id"], myStreamIterator):
    with open(output_transcription, 'a') as out:
        if resp["hyp_novar"] != "":
            sys.stdout.write("%s " % resp["hyp_novar"].strip())
            out.write("%s " % resp["hyp_novar"].strip())
            if resp["eos"] == True:
                sys.stdout.write("\n")
                out.write("\n")
            sys.stdout.flush()
